
  let studentList = [];

//activity17 function lists
   function addStudent(name){
	 studentList.push(name);
	  return `${name} is added to the student's list.`
    }

  function countStudents(){
	return `There are a total of ${studentList.length} students enrolled.`;
  }
	
    function printStudents(){
//sort 
	studentList.sort();

//print
	studentList.forEach(
		function(name){
			console.log(name);
		}
	)
}

   function findStudent(keyword){
	   let filteredArr =  studentList.filter(
							function(student){
								return student.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
							}
					);
	
	if (filteredArr.length == 1){
		return `${filteredArr.toString()} is an enrollee.`;
	} else if (filteredArr.length >= 2){
		return `${filteredArr.join(", ")} are enrollees.`;
	} else {
		return ` ${keyword} is not an enrollee.`;
	}
 
}

  function addSection(section){
	 return studentList.map(
			function(student){
				return student += ` - section ${section}`;
			}
	)
}

  function removeStudent(name){
	let modifiedName = name[0].toUpperCase() + name.slice(1);
	let iName = studentList.indexOf(modifiedName);
	let deletedStudent = studentList.splice(0,1);
	// console.log(modifiedName);
	// console.log(iName);

	return `${deletedStudent} was removed from the student's list.`
}




